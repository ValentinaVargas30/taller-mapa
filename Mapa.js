// Crear un objeto (variable)
//L representa a la bibioteca leaflet
let miMapa = L.map('mapid'); 

//Determinar la vista inicial
miMapa.setView([4.692896,-74.150819], 13);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')
miProveedor.addTo(miMapa);


//Crear un objeto marcador
let miMarcador = L.marker([4.628083,-74.154530]);
miMarcador.addTo(miMapa);


//JSON
let circle = L.circle([4.628083,-74.154530], {
    color: 'red',
    fillColor: 'red',
    fillOpacity: 0.5,
    radius: 100
});

var polygon = L.polygon([
    [ 4.628275297390873, -74.15472485125065],
    [ 4.628192420038865, -74.1546256095171],
    [ 4.628235863813306, -74.1545893996954],
    [ 4.628068772358532, -74.15439695119858],
    [ 4.627923068577783, -74.15452972054482],
    [ 4.62778137496406, -74.15435403585434],
    [ 4.6284363735069665, -74.15378540754318],
    [ 4.62856603641194, -74.1539416462183],
    [ 4.628622847471073, -74.153890684247],
    [ 4.628914923079572, -74.15422193706036],
    [ 4.628296685093053, -74.15475636720657],
    [ 4.628275297390873, -74.15472485125065]
    
]).addTo(miMapa);

circle.addTo(miMapa);

